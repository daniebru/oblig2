# Oblig 2

### Deltagere:
| Navn                | Studie | Studentnr. |
| --------------------|:------:|-----------:|
| Daniel Bruun        | BITSEC | 473117     |
| Svein Are Danielsen | BIDAT  | 253067     |
| Johan Aanesen       | BPROG | 473182     |

- Byttet chrome-prosess til dbus-prosess i dbusmajPF.bash

- Brukt shellcheck for testing av script