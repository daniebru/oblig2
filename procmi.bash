#! bin/bash

dato=$(date +"%d%m%y""-""%X")

for i in "$@"
do
fil="$i-$dato.meminfo"

data=$(grep VmData /proc/"$i"/status | awk '{ printf $2 }') #VmData
stk=$(grep VmStk /proc/"$i"/status | awk '{ printf $2 }')  #Stk
exe=$(grep VmExe /proc/"$i"/status | awk '{ printf $2 }')  #Exe

let var=$data+$stk+$exe

{
echo " ";
echo "********" Minne info om prosess "$i" "************";
echo Total bruk av VM: "$(grep VmSize /proc/"$i"/status | awk '{printf $2}')KB";
echo Mengde privat virtuelt minne "$var"KB " ";
echo Mengde virtuelt minne som er shared: "$(grep VmLib /proc/"$i"/status | awk ' { printf $2 }')"KB;
echo Total bruk av fysisk minne: "$(grep VmRSS /proc/"$i"/status | awk '{ printf $2 }')"KB;
echo Mengde fysisk minne benyttet til page table: "$(grep VmPTE /proc/"$i"/status | awk '{ printf $2 }')"KB
} >> "$fil"
done
