#! /bin/bash

arr=$(pgrep dbus)

for i in ${arr[*]}
do 
var=$(ps --no-header -o maj_flt $i)

if [ $var -gt 1000 ]
then
echo dbus $i har forårsaket $var major page faults "(Mer enn 1000!)"

else echo dbus $i har forårsaket $var major page faults
fi
done
