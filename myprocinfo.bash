#! /bin/bash

clear 

tall=0

while [ $tall != 9 ];  do

echo " "
echo Skriv tall 'for' valg av kommando
echo 1: Bruker og scriptnavn
echo 2: Siste boot
echo 3: Antall prosesser og tråder
echo 4: Context switcher siste sekund
echo 5: CPU-tid
echo 6: Interrupts
echo 9: Avslutt
echo " "
read -p "Skriv kommando  " tall
clear 

case $tall in 
1) echo Bruker: "$(whoami) scriptnavn: $0" ;;

2) echo Tid siden siste boot: "$(uptime -p)" ;;

3) echo Totalt prosesser: "$(ps -e --noheader | wc -l)" Tråder: "$(ps -eLf --noheader | wc -l)" ;;

4) echo Vent ett sekund:
	var1=$(grep ctxt /proc/stat | awk '{ print $2 }')
	sleep 1
	var2=$(grep ctxt /proc/stat | awk '{ print $2 }')
let var3=var2-var1 ;  clear
echo Context switcher siste sekund: $var3 ;;

5)   echo Vent ett sekund
    arr1=$(grep cpu /proc/stat | awk 'NR==1 { print $2, $3, $4 }') 
	sleep 1
    arr2=$(grep cpu /proc/stat | awk 'NR==1 { print $2, $3, $4 }') 
var1=var2=0
	for i in ${arr1[*]}
	do 
	let var1+=$i
	done

	for j in ${arr2[*]}
        do 
        let var2+=$j
        done

	let var=$var2-$var1
clear
echo Andel av CPU-tid i hundredeler benyttet i kernel og user mode siste sekund: $var ;;

6) echo Vent et sekund 
	var1="$(grep intr /proc/stat | awk ' {print $2 } ')"
	sleep 1
	var2="$(grep intr /proc/stat | awk ' {print $2 } ')"
	let var=$var2-$var1
	
	clear
	echo Antall interrupts siste sekund: $var ;;

9) clear ;;

*) echo Ukjent kommando. ;;

esac
done
